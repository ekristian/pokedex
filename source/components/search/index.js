import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'lodash';

class Search extends Component {
  componentDidMount() {
    this.onChange = this.onChange.bind(this);
    this.delayedCallback = debounce(this.delayedCallback, 500).bind(this);
  }

  delayedCallback(e) {
    if (this.props.onDoneTyping)
      this.props.onDoneTyping(e.target.value);
  }

  onChange(e) {
    e.persist();
    this.delayedCallback(e);
  }

  render() {
    return (
      <div className="searchBar">
        <div className="container">
          <img width="10" alt="Vector search icon" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Vector_search_icon.svg/8px-Vector_search_icon.svg.png"/>
          <input
            onChange={this.onChange}
            placeholder="Filter pokemon by name, ability or type"
          />
        </div>
      </div>
    );
  }
}

Search.propTypes = {
  onDoneTyping: PropTypes.func.isRequired
};

export default Search;
