var gulp = require('gulp'),
    requireDir = require('require-dir'),
    tasks = requireDir('./tasks'),
    gutil = require('gulp-util'),
    browserSync  = require('browser-sync'),
    livereload = require('gulp-livereload'),
    runSequence = require('run-sequence'),
    gzip = require('gulp-gzip');

gulp.task('buildScript:dev', tasks.scripts.buildScriptDev);
gulp.task('buildScript:dist', tasks.scripts.buildScriptDist);
gulp.task('copyHtml:dev', tasks.assets.htmlDev);
gulp.task('copyHtml:dist', tasks.assets.htmlDist);
gulp.task('copyFont:dev', tasks.assets.fontsDev);
gulp.task('copyFont:dist', tasks.assets.fontsDist);
gulp.task('style:dev', tasks.styles.lessDev);
gulp.task('style:dist', tasks.styles.lessDist);
gulp.task('minifyCSS', ['style:dist'], tasks.styles.minifyCSS);

gulp.task('gzip', function() {
    gulp.src(tasks.config.buildDir + '/**/*.{js,html,css}',  { base: "./" })
    .pipe(gzip())
    .pipe(gulp.dest('.'));
});

//watch actions
gulp.task('watch:scripts', ['buildScript:dev'], function(done) {
  setTimeout(function() { browserSync.reload() }, 200);
  done();
});

gulp.task('watch:styles', ['style:dev'], function(done) {
  // browserSync.reload();
  done();
});

gulp.task('watch', function() {
  // gulp.watch(tasks.config.sourceDir + '/**/*.js', ['watch:scripts']);
  gulp.watch(tasks.config.sourceDir + '/**/*.less', ['watch:styles']);
  gulp.watch(tasks.config.sourceDir + '/index.html', ['copyHtml:dev']);

  gulp.watch(tasks.config.developmentDir + '/assets/*.css', function() {
    gulp.src(tasks.config.developmentDir + '/assets/*.css')
      .pipe(browserSync.reload({ stream:true }));
  });
});

gulp.task('browsersync', function() {
  browserSync.init({
    server: {
      baseDir: tasks.config.developmentDir
    }
  });
});

gulp.task('serve', [
  'copyFont:dev', 'browsersync', 'buildScript:dev', 'style:dev',
  'copyHtml:dev', 'watch']);

gulp.task('prepareBuild', [
  'copyFont:dist', 'buildScript:dist', 'minifyCSS',
  'copyHtml:dist']);

gulp.task('build', function(callback) {
  runSequence('prepareBuild', ['gzip'], callback);
});
