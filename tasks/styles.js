var gulp = require('gulp'),
    requireDir = require('require-dir'),
    less = require('gulp-less'),
    _tasks = requireDir('../tasks'),
    gutil = require('gulp-util')
    path = require('path')
    cleanCSS = require('gulp-clean-css');

module.exports = {
  lessDev: function () {
    var l = less({
        paths: [ path.join(__dirname, 'less', 'includes') ]
    });

    l.on('error',function(e){
      gutil.log(e);
      l.end();
    });

    return gulp.src(_tasks.config.sourceDir + '/**/*.less')
      .pipe(l)
      .pipe(gulp.dest(_tasks.config.developmentDir));
  },
  lessDist: function () {
    var l = less({
        paths: [ path.join(__dirname, 'less', 'includes') ]
    });

    l.on('error',function(e){
      gutil.log(e);
      l.end();
    });

    return gulp.src(_tasks.config.sourceDir + '/**/*.less')
      .pipe(l)
      .pipe(gulp.dest(_tasks.config.buildDir));
  },
  minifyCSS: function() {
    return gulp.src(_tasks.config.buildDir+ '/**/*.css', {base: './'})
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(gulp.dest('.'));
  }
}
