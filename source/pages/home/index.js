import React, { Component } from 'react';
import {
  Header, Footer, Search, PokemonCard, Scrollable
} from '../../components';
import PokemonService from '../../services/pokemon';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }

    this.onDoneTyping = this.onDoneTyping.bind(this);
    this.onReachEnd = this.onReachEnd.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  getData(offset = 0) {
    this.processed = true;
    PokemonService.getPokemons(offset)
      .then(res => {
        if (res && res.data) {
          this.processed = false;
          this.setState({data: res.data});
        }
      })
  }

  onReachEnd() {
    if (!this.processed) {
      this.getData(this.state.data.length);
    }
  }

  onDoneTyping(value) {
    this.setState({filtered: value});
  }

  renderChild() {
    let views = [];
    let iLength = this.state.data.length;

    for (let i = 0; i < iLength; i++) {
      let item = this.state.data[i];
      views.push(
        <PokemonCard
          filtered={this.state.filtered}
          key={`pokemon-${i}`}
          pokemon={item}
        />
      )
    }

    return views;
  }

  render() {
    return (
      <div>
        <Header />
        <div className="main-content">
          <Search onDoneTyping={this.onDoneTyping}/>
          <Scrollable
            onReachEnd={this.onReachEnd}
            className="container cards"
          >
            { this.renderChild() }
          </Scrollable>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Home;
