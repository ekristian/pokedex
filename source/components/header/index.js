import React from 'react';

const Header = () => {
  return (
    <div className="header">
      <div className="container">
        Pokedex
      </div>
    </div>
  );
};

export default Header;
