import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

class Scrollable extends Component {
  componentDidMount() {
    document.addEventListener('scroll', event => {
      let scrollTop = window.pageYOffset || document.documentElement.scrollTop ||
                      document.body.scrollTop || 0;

      if (document.body.scrollHeight == (scrollTop + window.innerHeight)) {
        // scroll reach end;
        if (this.props.onReachEnd) this.props.onReachEnd();
      }
    });
  }

  render() {
    return (
      <div className={this.props.className}>
        { this.props.children }
      </div>
    );
  }
}

Scrollable.propTypes = {
  onReachEnd: PropTypes.func,
  onScroll: PropTypes.func
};

export default Scrollable;
