var gulp = require('gulp')
    requireDir = require('require-dir'),
    _tasks = requireDir('../tasks');

module.exports = {
  htmlDev: function() {
    return gulp.src(_tasks.config.sourceDir + '/index.html')
               .pipe(gulp.dest(_tasks.config.developmentDir));
  },
  htmlDist: function() {
    return gulp.src(_tasks.config.sourceDir + '/index.html')
               .pipe(gulp.dest(_tasks.config.buildDir));
  },

  fontsDev: function() {
    return gulp.src(_tasks.config.sourceDir + '/assets/fonts/**')
               .pipe(gulp.dest(_tasks.config.developmentDir + '/assets/fonts/'));
  },
  fontsDist: function() {
    return gulp.src(_tasks.config.sourceDir + '/assets/fonts/**')
               .pipe(gulp.dest(_tasks.config.buildDir + '/assets/fonts/'));
  },
};
