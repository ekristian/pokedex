import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import { HashRouter } from 'react-router-dom';
import axios from 'axios';
import Routes from './routes';

class App extends Component {
  constructor(props) {
    super(props);

    axios.defaults.baseURL = 'http://pokeapi.salestock.net/api/v2/';
  }

  render() {
    return (
      <div>
        <Routes />
      </div>
    );
  }
}

ReactDOM.render(
  <HashRouter>
    <App />
  </HashRouter>,
  document.getElementById('root')
);
