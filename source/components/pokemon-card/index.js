import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Avatar from './avatar';
import Detail from './detail';
import { isEqual } from 'lodash';
import PokemonService from '../../services/pokemon';

class PokemonCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemon: props.pokemon,
      filtered: props.filtered,
      img: '',
      detail: {},
      showDetail: false
    }

    this.onCloseRequested = this.onCloseRequested.bind(this);
    this.openDetail = this.openDetail.bind(this);
    this.isHide = props.filtered && props.filtered !== '';
  }

  componentWillReceiveProps(nextProps) {
    if (!isEqual(nextProps.pokemon, this.state.pokemon)) {

      let { pokemon } = nextProps;
      this.setState({pokemon});
      this.getDetailWhenVisible(pokemon)
    }

    if (!isEqual(nextProps.filtered, this.state.filtered)) {
      this.setState({filtered: nextProps.filtered});
      this.doFilter(this.state.detail, nextProps.filtered);
    }
  }

  componentDidMount() {
    this.isHide = false;
    this.getDetailWhenVisible();
    document.addEventListener('scroll', event => {
      this.getDetailWhenVisible();
    });
  }

  doFilter(detail, filtered) {
    let isHide = true;
    let lowerCaseFiltered = (filtered || '').toLowerCase();
    if (this.state.pokemon.name.toLowerCase().indexOf(lowerCaseFiltered) > -1)
      isHide = false;

    if (detail && !isEqual(detail, {})) {
      if (isHide) {
        let lgth = detail.abilities.length;
        for (let i = 0; i < lgth; i++) {
          let ability = detail.abilities[i].ability;
          if (ability.name.toLowerCase().indexOf(lowerCaseFiltered) > -1)
            isHide = false;
        }
      }

      if (isHide) {
        let lgth = detail.types.length;
        for (let i = 0; i < lgth; i++) {
          let type = detail.types[i].type;
          if (type.name.toLowerCase().indexOf(lowerCaseFiltered) > -1)
            isHide = false;
        }
      }
    }

    if (isHide !== this.isHide) {
      this.isHide = isHide;
      this.forceUpdate();
      setTimeout(() => {
        this.getDetailWhenVisible();
      }, 200);
    }
  }

  getDetailWhenVisible(pokemon = this.state.pokemon) {
    let el = this.isInViewport();

    if (el && !this.processed) {
      this.processed = true;
      this.getDetail(pokemon);
    }
  }

  getId(data) {
    let tmp = data.split('/');
    return tmp[tmp.length-2];
  }

  getDetail(pokemon) {
    if (pokemon && pokemon.url) {
      let id = this.getId(pokemon.url);
      PokemonService.getPokemonDetail(id)
        .then(res => {
          if (res && res.data) {
            this.setState({
              img: res.data.sprites.front_default,
              detail: res.data
            });
            this.doFilter(res.data, this.state.filtered);
          }
        });
    }
  }

  getHP(detail = this.state.detail) {
    if (detail.stats) {
      return detail.stats[detail.stats.length-1].base_stat
    }
    return 0;
  }

  isInViewport(offset = -80) {
    if (!this.thisEl) return false;
    const top = this.thisEl.getBoundingClientRect().top;
    return (top + offset) >= 0 && (top - offset) <= window.innerHeight;
  }

  openDetail() {
    if (!isEqual({}, this.state.detail)) {
      this.setState({showDetail: true});
    }
  }

  onCloseRequested() {
    this.setState({showDetail: false});
  }

  render() {
    if (this.isHide) return null;
    return (
      <div
        ref={(el) => this.thisEl = el}
        onClick={this.openDetail}
        className={["pokemon-card", (!isEqual({}, this.state.detail)?'clickable':'')].join(' ')}
      >
        <Avatar img={this.state.img}/>
        <div className="pokemon-detail">
          <p className="title">{ this.state.pokemon.name }</p>

          <div className="details-item">
            <div className="item">
              <p className="value">{this.getHP()}</p>
              <p className="type">HP</p>
            </div>
            <div className="item">
              <p className="value">{this.state.detail.weight}kg</p>
              <p className="type">Weight</p>
            </div>
            <div className="item">
              <p className="value">{this.state.detail.height}m</p>
              <p className="type">Height</p>
            </div>

          </div>
        </div>
        <Detail
          getHP={this.getHP}
          onCloseRequested={this.onCloseRequested}
          detail={this.state.detail}
          show={this.state.showDetail}
        />
      </div>
    );
  }
}

PokemonCard.propTypes = {
  pokemon: PropTypes.object.isRequired,
  filtered: PropTypes.string
};

export default PokemonCard;
