import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Avatar extends Component {
  render() {
    return (
      <div className={["avatar", this.props.img === '' ? 'backgrounded': ''].join(' ')}>
        <img src={this.props.img} />
      </div>
    );
  }
}

Avatar.propTypes = {
  img: PropTypes.string.isRequired
};

export default Avatar;
