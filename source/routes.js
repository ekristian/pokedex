import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import Home from './pages/home';
import Error404 from './pages/errors/404';

class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route path="*" component={Error404} />
      </Switch>
    );
  }
}

export default withRouter(Routes);
