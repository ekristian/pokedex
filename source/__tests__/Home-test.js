import React from 'react';
import { shallow, mount, render } from 'enzyme';

import Home from '../pages/home';

describe('A suite', function() {
  it('should render without throwing an error', function() {
    expect(render(<Home />).text()).toEqual('Pokedexevkristian@gmail.com');
  });
});
