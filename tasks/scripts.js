var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    rename = require('gulp-rename'),
    streamify = require('gulp-streamify')
    requireDir = require('require-dir'),
    _tasks = requireDir('../tasks')
    gutil = require('gulp-util'),
    fs = require('fs')
    watchify = require('watchify'),
    browserSync  = require('browser-sync'),
    notify = require("gulp-notify");

var reload = browserSync.reload;

module.exports = {
  buildScriptDist: function () {
    process.env.NODE_ENV = 'production';
    return browserify(_tasks.config.sourceDir + '/index.js')
      .transform(babelify, {presets: ['es2015', 'react']})
      .bundle()
      .pipe(source('app.js'))
      .pipe(gulp.dest(_tasks.config.buildDir + '/assets'))
      .pipe(rename('app.min.js'))
      .pipe(streamify(concat('app.min.js')))
      .pipe(streamify(uglify()))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(_tasks.config.buildDir + '/assets'));
  },
  buildScriptDev: function () {
    var bundler = watchify(browserify(_tasks.config.sourceDir + '/index.js', watchify.args));
        bundler.transform(babelify, {presets: ['es2015', 'react']});
        bundler.on('update', rebundle);

    function rebundle() {
      return bundler.bundle()
        // log errors if they happen
        .on('error', function(err){
          // print the error (can replace with gulp-util)
          console.log(err.message);
          this.emit('end');
        })
        .pipe(source('app.min.js'))
        .pipe(gulp.dest(_tasks.config.developmentDir + '/assets'))
        .pipe(reload({stream:true}));
        // .pipe(notify("Browser reloaded after watchify update!"));
    }

    return rebundle();
  }
};
