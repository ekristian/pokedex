import axios from 'axios';
import Singleton from './singleton';
import Q from 'q';

const perpage = 21;

const PokemonService = {
  getPokemons: (offset = 0) => {
    const deferred = Q.defer();

    let application = Singleton.instance;
    let pokemons = application.getData('pokemons') || [];

    if (pokemons.length && pokemons.length > offset) {
      deferred.resolve({ data: pokemons});
    } else {
      axios.get(`/pokemon?limit=${perpage}&offset=${offset}`)
        .then((res) => {
          application.setData('pokemons', pokemons.concat(res.data.results));
          deferred.resolve({data: pokemons.concat(res.data.results)});
        }, () => {
          deferred.resolve({ data: [] });
        });
    }

    return deferred.promise;
  },

  getPokemonDetail: id => {
    const deferred = Q.defer();

    let application = Singleton.instance;
    let pokemon = application.getData(`pokemon-${id}`);

    if (pokemon) {
      deferred.resolve({ data: pokemon});
    } else {
      axios.get(`/pokemon/${id}`)
        .then((res) => {
          application.setData(`pokemon-${id}`, res.data);
          deferred.resolve({ data: res.data });
        }, () => {
          deferred.resolve({ data: [] });
        });
    }

    return deferred.promise;
  }
};

export default PokemonService;
