import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Detail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show: props.show
    };

    this.requestClose = this.requestClose.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.show !== this.state.show)
      this.setState({show: nextProps.show});
  }

  preventClose(evt) {
    evt.stopPropagation();
  }

  requestClose() {
    this.setState({show: false});
    this.props.onCloseRequested();
  }

  renderImages() {
    let views = [];
    let sprites = this.props.detail.sprites;

    for(var key in sprites) {
      if(sprites.hasOwnProperty(key)) {
        let sprite = sprites[key];
        if (sprite) {
          views.push(
            <img key={key} src={sprite} />
          );
        }
      }
    }

    return views;
  }

  showType() {
    if (!this.props.detail) return null;
    let view = '';

    for (var i = 0; i < this.props.detail.types.length; i++) {
      let type = this.props.detail.types[i].type;
      if (type.name) {
        if (i === 0) view += type.name;
        else view += `/${type.name}`;
      }
    }

    return view;
  }

  showAbilities() {
    if (!this.props.detail) return null;
    let view = '';

    for (var i = 0; i < this.props.detail.abilities.length; i++) {
      let ability = this.props.detail.abilities[i].ability;
      if (ability.name) {
        if (i === 0) view += ability.name;
        else view += `/${ability.name}`;
      }
    }

    return view;
  }

  render() {
    if (!this.state.show) return null;
    return (
      <div className={'popup-pokemon'} onClick={this.requestClose}>
        <div className={'inner'} onClick={this.preventClose}>
          <p className="name">{this.props.detail.name}</p>

          <div className={'pictures'}>
            { this.renderImages() }
          </div>

          <div className="details-item">
            <div className="item">
              <p className="value">{this.props.getHP(this.props.detail)}</p>
              <p className="type">HP</p>
            </div>
            <div className="item">
              <p className="value">{this.props.detail.weight}kg</p>
              <p className="type">Weight</p>
            </div>
            <div className="item">
              <p className="value">{this.props.detail.height}m</p>
              <p className="type">Height</p>
            </div>

            <div className="item">
              <p className="value">{this.showType()}</p>
              <p className="type">Type</p>
            </div>
            <div className="item">
              <p className="value">{this.showAbilities()}</p>
              <p className="type">Abilities</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Detail.propTypes = {
  detail: PropTypes.object.isRequired,
  show: PropTypes.bool.isRequired,
  onCloseRequested: PropTypes.func.isRequired
};

export default Detail;
